/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.UserAgent;
import net.coalevo.foundation.model.SessionListener;
import net.coalevo.foundation.model.Session;
import net.coalevo.foundation.util.srp.ServerSRP;
import net.coalevo.messaging.service.MessagingService;
import net.coalevo.presence.model.Presence;
import net.coalevo.presence.service.PresenceService;
import net.coalevo.security.service.SecurityService;
import net.coalevo.system.service.SessionService;
import net.coalevo.empp.server.model.NetworkSession;
import org.apache.mina.common.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Provides a network session implementation.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class NetworkSessionImpl implements NetworkSession {

  //private static final Logger log = LoggerFactory.getLogger(NetworkSessionImpl.class);

  private AgentIdentifier m_AgentIdentifier;
  private AtomicBoolean m_Authenticated;
  private ServerSRP m_ServerSRP;
  private UserAgent m_Agent;
  private boolean m_Valid;
  private Presence m_Presence;
  private String m_SessionIdentifier;
  private IoSession m_IoSession;

  public NetworkSessionImpl(AgentIdentifier from) {
    m_AgentIdentifier = from;
    m_Authenticated = new AtomicBoolean(false);
    m_ServerSRP = getServerSRP(from);
  }//NetworkSessionImpl

  public ServerSRP getSRP() {
    return m_ServerSRP;
  }//getSRP

  public boolean isAuthenticated() {
    return m_Authenticated.get();
  }//isInitiated

  public String getSessionIdentifier() {
    return m_SessionIdentifier;
  }//getSessionIdentifier

  public void setAuthenticated(boolean b, IoSession ios, String id) {
    m_Authenticated.set(b);
    if (b) {
      m_SessionIdentifier = id;
      SecurityService sec =
          Activator.getServices().getSecurityService(ServiceMediator.NO_WAIT);
      SessionService ses =
          Activator.getServices().getSessionService(ServiceMediator.NO_WAIT);
      PresenceService ps =
          Activator.getServices().getPresenceService(ServiceMediator.NO_WAIT);
      MessagingService ms =
          Activator.getServices().getMessagingService(ServiceMediator.NO_WAIT);
      m_IoSession = ios;

      //1. Get UserAgent
      m_Agent = sec.getUserAgent(m_ServerSRP);
      Activator.log().debug("Got Agent" + m_Agent.toString());
      //2. Initiate Session and add a listener to invalidate
      Session s = ses.initiateSession(m_Agent, 60 * 90, TimeUnit.SECONDS); //this will work no worry
      Activator.log().debug("initiatingSession()");
        s.addSessionListener(
            new SessionListener() {
              public void invalidated(Session s) {
                NetworkSessionImpl.this.invalidate();
              }//invalidated
            }//anonymous SessionListener
        );
      //3. Register with Presence
      m_Presence = ps.register(m_Agent, new PresenceServiceListenerImpl(ios,id));
      Activator.log().debug("registered presence " + m_Presence.toString());
      //4. Register with Messaging
      ms.register(m_Presence, new MessagingServiceListenerImpl(ios, m_AgentIdentifier,id));
      Activator.log().debug("registered messaging ");

      m_Valid = true;
    } else {
      invalidate();
    }
  }//setAuthenticated

  public void activity() {
    if(m_Agent!= null) {
      net.coalevo.foundation.model.Session s = m_Agent.getSession();
      if(s != null) {
        s.access();
      }
    }
  }//activity

  public AgentIdentifier getAgentIdentifier() {
    return m_AgentIdentifier;
  }//getAgentIdentifier

  public UserAgent getAgent() {
    return m_Agent;
  }//getAgent

  public Presence getPresence() {
    return m_Presence;
  }//getPresence

  public ServerSRP getServerSRP(AgentIdentifier aid)
      throws SecurityException {
    SecurityService sec =
        Activator.getServices().getSecurityService(ServiceMediator.NO_WAIT);
    return sec.getServerSRP(aid.toString());
  }//getServerSRP

  public void invalidate() {
    if (m_Valid) {
      m_Valid = false;
      m_Authenticated.set(false);
      //The call is save, even if this was called from the session invalidation
      //listener
      m_Agent.getSession().invalidate();
      if(m_IoSession.isConnected() && !m_IoSession.isClosing()) {
        m_IoSession.close();
      }
      m_AgentIdentifier = null;
      m_ServerSRP = null;
      m_Agent = null;

      /*
      //technically the session listener of the presence and messagin service
      //take care of this.
      try {
        Activator.getServices().getMessagingService(ServiceMediator.NO_WAIT).unregister(m_Presence);
      } catch (Exception ex) {

      }
      if(m_Presence.isPresent()) {
        m_Presence.end();
      }
      try {
        Activator.getServices().getPresenceService(ServiceMediator.NO_WAIT).unregister(m_Presence);
      } catch (Exception ex) {

      }

      */
    }
  }//invalidate

}//class NetworkSessionImpl
