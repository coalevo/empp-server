/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.impl;

import net.coalevo.empp.server.service.EMPPServerConfiguration;
import net.coalevo.empp.server.service.ExtensionManager;
import net.coalevo.empp.server.ext.NicknameExtension;
import net.coalevo.empp.server.ext.RolesExtension;
import net.coalevo.empp.server.model.ServerExtension;
import net.coalevo.empp.io.StreamFactory;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.service.MessageResourceService;
import net.coalevo.foundation.util.BundleConfiguration;
import net.coalevo.logging.model.LogProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import java.util.concurrent.atomic.AtomicBoolean;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;


/**
 * Provides the implementation of the OSGi <tt>BundleActivator</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator implements BundleActivator {

  private static LogProxy c_Log;
  private static Marker c_LogMarker;
  private AtomicBoolean m_Started = new AtomicBoolean(false);
  private static BundleContext c_BundleContext;
  private static ServiceMediator c_Services;
  private static long c_StartTime;
  private static Messages c_BundleMessages;
  private static BundleConfiguration c_BundleConfiguration;
  private static EMPPServer c_EMPPServer;
  private static AgentIdentifier c_ServerIdentifier = new AgentIdentifier("");


  public void start(final BundleContext bundleContext)
      throws Exception {
    if (m_Started.get()) {
      throw new Exception("Already started.");
    }
    c_BundleContext = bundleContext;
    Thread t = new Thread(new Runnable() {

      public void run() {
        //1. Log
        c_LogMarker = MarkerFactory.getMarker(Activator.class.getName());
        c_Log = new LogProxy();
        c_Log.activate(bundleContext);

        //2. prepare service mediator
        c_Services = new ServiceMediator();
        c_Services.activate(c_BundleContext);
        //2. Get Messages
        MessageResourceService mrs =
            c_Services.getMessageResourceService(ServiceMediator.WAIT_UNLIMITED);
        if (mrs == null) {
          log().error("MessageResourceService unavailable.");
          return;
        }
        c_BundleMessages = mrs.getBundleMessages(c_BundleContext.getBundle());
        if (c_BundleMessages == null) {
          log().error("Bundle Message Resources could not be resolved.");
          return;
        }
        //1. BundleConfiguration
        c_BundleConfiguration = new BundleConfiguration(EMPPServerConfiguration.class.getName());
        c_BundleConfiguration.activate(c_BundleContext);
        c_Services.setConfigMediator(c_BundleConfiguration.getConfigurationMediator());

        //2. ExtensionManager
        ExtensionManagerImpl mgr = new ExtensionManagerImpl();
        mgr.activate(c_BundleContext);
        c_BundleContext.registerService(
            ExtensionManager.class.getName(),
            mgr,
            null);
        //3. Internally Implemented Extensions
        c_BundleContext.registerService(
            ServerExtension.class.getName(),
            new NicknameExtension(),
            null);
        c_BundleContext.registerService(
            ServerExtension.class.getName(),
            new RolesExtension(),
            null);

        try {
          c_EMPPServer = new EMPPServer(
              c_BundleConfiguration.getConfigurationMediator().getConfiguration()
                  .getInteger(EMPPServerConfiguration.EMPP_SERVER_PORT_KEY));
          c_EMPPServer.activate(c_BundleContext);
          c_EMPPServer.start();
          c_BundleConfiguration.getConfigurationMediator().addUpdateHandler(c_EMPPServer);
        } catch (Exception ex) {
          log().error("start()::run()", ex);
        }

        c_StartTime = System.currentTimeMillis();
        log().info(c_BundleMessages.get("Activator.started"));

      }//run

    }//Runnable
    );//thread
    t.setContextClassLoader(bundleContext.getBundle().getClass().getClassLoader());
    t.start();
  }//start

  public void stop(BundleContext bundleContext) throws Exception {
    if(c_EMPPServer != null) {
    c_EMPPServer.stop();
    c_EMPPServer.deactivate();
    }
    if (c_Services != null) {
      c_Services.deactivate();
    }
    c_EMPPServer = null;
    c_BundleConfiguration = null;
    log().info(c_BundleMessages.get("Activator.stopped"));


    if (c_Log != null) {
      c_Log.deactivate();
      c_Log = null;
    }
    c_LogMarker = null;
    c_BundleMessages = null;
    c_Services = null;
    c_BundleContext = null;
  }//stop

  public static ServiceMediator getServices() {
    return c_Services;
  }//getServices

  public static long getStartTime() {
    return c_StartTime;
  }//getStartTime

  public static Messages getBundleMessages() {
    return c_BundleMessages;
  }//getBundleMessages

  public static AgentIdentifier getServerIdentifier() {
    return c_ServerIdentifier;
  }//getServerIdentifier

  public static PacketHandler getPacketHandler() {
    return c_EMPPServer.getPacketHandler();
  }//getPacketHandler

   /**
   * Return the bundles logger.
   *
   * @return the <tt>Logger</tt>.
   */
  public static Logger log() {
    return c_Log;
  }//log

}//class Activator
