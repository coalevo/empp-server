/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.impl;

import net.coalevo.empp.impl.InstantMessageTypes;
import net.coalevo.empp.impl.MessageEventTypes;
import net.coalevo.empp.impl.Packets;
import net.coalevo.empp.model.*;
import net.coalevo.messaging.model.EditableInteractiveMessage;
import net.coalevo.messaging.model.InteractiveMessageType;
import net.coalevo.messaging.model.InteractiveMessageTypes;
import net.coalevo.messaging.service.MessagingService;
import net.coalevo.presence.model.Presence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.mina.common.IoSession;

import java.util.Iterator;
import java.util.Map;

/**
 * Provides a handler for incoming {@link InstantMessage} packets.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MessageHandler
    extends ExtensiblePacketHandler {

  //private static final Logger log = LoggerFactory.getLogger(MessageHandler.class);
  private MessagingService m_MessagingService;

  public MessageHandler() {
    m_MessagingService = Activator.getServices().getMessagingService(ServiceMediator.NO_WAIT);
    if(m_MessagingService == null) {
      Activator.log().error("MessagingService NULL!");
      m_MessagingService = Activator.getServices().getMessagingService(ServiceMediator.WAIT_UNLIMITED);
    }
  }//constructor

  public void handleMessagePacket(IoSession io, NetworkSessionImpl ns, InstantMessage m) {
    try {
      InstantMessageType type = (InstantMessageType) m.getType();
      Presence pres = ns.getPresence();
      if (InstantMessageTypes.CHAT.equals(type)
          || InstantMessageTypes.GROUPCHAT.equals(type)
          || InstantMessageTypes.NOTIFICATION.equals(type)
          || InstantMessageTypes.NEWS.equals(type)) {
        EditableInteractiveMessage mes =
            m_MessagingService.create(pres, translateType(type), m.getIdentifier());
        //copy over
        mes.setTo(m.getTo());
        mes.setType(translateType(type));
        mes.setThread(m.getThread());
        if (m.hasSubjects()) {
          for (Iterator iterator = m.getSubjects(); iterator.hasNext();) {
            Map.Entry entry = (Map.Entry) iterator.next();
            mes.setSubject(((Language) entry.getKey()).toLocale(), (String) entry.getValue());
          }
        }
        if (m.hasBodies()) {
          for (Iterator iterator = m.getBodies(); iterator.hasNext();) {
            Map.Entry entry = (Map.Entry) iterator.next();
            mes.setBody(((Language) entry.getKey()).toLocale(), (String) entry.getValue());
          }
        }
        mes.setConfirmationRequired(m.isConfirmationRequired());
        m_MessagingService.send(pres, mes);
      } else if (InstantMessageTypes.EVENT.equals(type)) {
        MessageEventType et = m.getEventType();
        if (MessageEventTypes.MSG_START.equals(et)) {
          m_MessagingService.started(
              pres,
              new EventInfoImpl(m.getIdentifier(), m.getFrom(), m.getTo())
          );
        } else if (MessageEventTypes.MSG_STOP.equals(et)) {
          m_MessagingService.stopped(
              pres,
              new EventInfoImpl(m.getIdentifier(), m.getFrom(), m.getTo())
          );
        }
      } else if(InstantMessageTypes.CONFIRMATION.equals(type)) {
        //confirming message
        m_MessagingService.confirm(
            pres,
            //TODO: We need to swap this, but maybe it should be made
            //clearer how the confirmation works.
            new EventInfoImpl(m.getIdentifier(), m.getTo(), m.getFrom())
        );
      } else {
        return;

      }
      //TODO: Means that there will be no response to any of these extensions
      if(m.hasExtensions()) {
        handleExtensions(ns,m);
      }
    } catch (Exception ex) {
      Activator.log().error("handleMessagePacket(InstantMessage)",ex);
    } finally {
      Packets.releasePacket(m);
    }
  }//handleMessagePacket

  public void handleMessagePacket(IoSession io, NetworkSessionImpl ns, IQ iq) {


  }//handleMessagePacket

  private InteractiveMessageType translateType(InstantMessageType type) {
    if (InstantMessageTypes.CHAT.equals(type)) {
      return InteractiveMessageTypes.CHAT;
    } else if (InstantMessageTypes.GROUPCHAT.equals(type)) {
      return InteractiveMessageTypes.GROUPCHAT;
    } else if (InstantMessageTypes.NOTIFICATION.equals(type)) {
      return InteractiveMessageTypes.NOTIFICATION;
    } else if (InteractiveMessageTypes.NEWS.equals(type)) {
      return InteractiveMessageTypes.NEWS;
    } else {
      return null;
    }
  }//translateType

}//class MessageHandler
