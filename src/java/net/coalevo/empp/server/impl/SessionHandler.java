/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.impl;

import net.coalevo.empp.model.Session;
import net.coalevo.empp.impl.SessionTypes;
import net.coalevo.empp.io.TSFilter;
import net.coalevo.foundation.util.srp.ServerSRP;
import net.coalevo.foundation.util.crypto.KDF;
import org.apache.mina.common.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Provides the handler for {@link Session} packets.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class SessionHandler {

  //private static final Logger log = LoggerFactory.getLogger(SessionHandler.class);
  private Map<String, NetworkSessionImpl> m_Sessions;

  public SessionHandler() {
    m_Sessions = new ConcurrentHashMap<String,NetworkSessionImpl>(100);
  }//constructor

  //todo: This is a request response schema, there should be some timeout.
  public void handleSessionPacket(IoSession ios, Session s) throws Exception {
    //just accept client packages
    if(SessionTypes.CLIENT.equals(s.getType())) {
      NetworkSessionImpl ns = getSession(s.getIdentifier());
      if(ns == null) {
        //new session and add
        ns = new NetworkSessionImpl(s.getFrom());
        m_Sessions.put(s.getIdentifier(),ns);
        //set public client ephemeral
        ServerSRP sSRP = ns.getSRP();
        sSRP.received(s.getPublicEphemeral());
        //return session with ephemeral and salt
        s.setSalt(sSRP.getSalt());
        s.setPublicEphemeral(sSRP.getPublicEphemeral());
        s.setType(SessionTypes.SERVER);
        s.setTo(s.getFrom());
        s.setFrom(Activator.getServerIdentifier());
        s.preparePayload();
        ios.write(s);
      } else {
        if(s.getMatchProof() != null) {
          ServerSRP sSRP = ns.getSRP();
          if(sSRP.checkClientKeyProof(s.getMatchProof())) {
            ns.setAuthenticated(true,ios,s.getIdentifier());
            s.setMatchProof(sSRP.getKeyMatchProof());
            boolean encrypt = s.isEncryptRequested();
            if (encrypt) {
              s.respondEncrypt(true);
              s.createKDFIndex();
               //start encrypt
              KDF kdf = new KDF();
              Activator.log().debug("KDF Index = " + s.getKDFIndex());
              Activator.log().debug("Session Key = " + java.util.Arrays.toString(sSRP.getSessionKey()));
              Activator.log().debug("Keylen = " + s.getKeyLength());
              kdf.init(sSRP.getSessionKey(), s.getKDFIndex());
              int keylen = s.getKeyLength();
              //activate session attributes
              ios.setAttribute(TSFilter.TS_MACKEY, kdf.nextKey(keylen));
              ios.setAttribute(TSFilter.TS_CRYPTKEY, kdf.nextKey(keylen));
              Activator.log().debug("MAC Key = " + java.util.Arrays.toString((byte[])ios.getAttribute(TSFilter.TS_MACKEY)));
              Activator.log().debug("Crypt Key = " + java.util.Arrays.toString((byte[])ios.getAttribute(TSFilter.TS_CRYPTKEY)));


              ios.setAttribute(TSFilter.TS_DONT_ENCRYPT_NEXT,true);
              ios.setAttribute(TSFilter.TS_ENABLED, true);
              Activator.log().debug("Enabling encryption");

              ios.setAttribute(EMPP_SESSION,s.getIdentifier());
              //add filter
              Activator.log().debug("Authenticated " + s.getIdentifier().toString());
              Activator.log().debug("created and set session keys");

            }
            s.setType(SessionTypes.SERVER);
            s.setTo(s.getFrom());
            s.setFrom(Activator.getServerIdentifier());
            s.preparePayload();
            ios.write(s);
          } else {
            //close down
            Activator.log().error(
                Activator.getBundleMessages().get(
                    "SessionHandler.session.failed","client",ios.toString()));
            ios.close();
          }
        }
      }
    } else {
      throw new Exception("SessionHandler.invalidtype");
    }
  }//handleSessionPacket

  public NetworkSessionImpl getSession(String id) {
    return m_Sessions.get(id);
  }//getSession

  public void sessionClosed(String id) {
    NetworkSessionImpl ns = m_Sessions.get(id);
    if(ns != null) {
      m_Sessions.remove(id);
      ns.invalidate();
    }
  }//sessionClosed

  public boolean isValidSession(String id) {
    return m_Sessions.containsKey(id);
  }//isValidSession

   public static final String EMPP_SESSION= "empp.session";

}//class SessionHandler
