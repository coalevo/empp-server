/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.impl;

import net.coalevo.empp.model.ExtensibleStanza;
import net.coalevo.empp.model.NamespaceExtension;
import net.coalevo.empp.server.model.NamespaceExtensionHandler;

import java.util.*;

/**
 * Generic base class for a packet handler that allows to
 * register and handle extensions through the registered
 * {@link NamespaceExtensionHandler} instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
abstract class ExtensiblePacketHandler {

  private Map<String,NamespaceExtensionHandler> m_Handlers;

  public ExtensiblePacketHandler() {
    m_Handlers = new HashMap<String,NamespaceExtensionHandler>();
  }//ExtensiblePacketHandler

  public NamespaceExtensionHandler getHandlerFor(String namespace) {
    return m_Handlers.get(namespace);
  }//getHandlerFor

  public boolean hasHandlerFor(String namespace) {
    return m_Handlers.containsKey(namespace);
  }//getHandlerFor

  public boolean registerHandler(String namespace, NamespaceExtensionHandler handler) {
    if(!hasHandlerFor(namespace)) {
      m_Handlers.put(namespace,handler);
      return true;
    } else {
      return false;
    }
  }//registerHandler

  public void unregisterHandler(String namespace) {
    m_Handlers.remove(namespace);
  }//unregisterHandler

  public List<NamespaceExtension> handleExtensions(NetworkSessionImpl ns,ExtensibleStanza req) {
    List<NamespaceExtension> res = new ArrayList<NamespaceExtension>();
    for(Iterator<String> iter = req.getExtensions(); iter.hasNext();) {
      String namespace = iter.next();
      NamespaceExtensionHandler handler = m_Handlers.get(namespace);
      NamespaceExtension nex = handler.handleExtension(ns,req.getExtension(namespace));
      if(nex!=null) {
        res.add(nex);
      }
    }
    return res;
  }//handleExtensions

}//class ExtensiblePacketHandler
