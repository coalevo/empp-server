/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.impl;

import net.coalevo.empp.impl.Stanzas;
import net.coalevo.empp.server.model.*;
import net.coalevo.empp.server.service.ExtensionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.osgi.framework.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Provides an implementation of {@link ExtensionManager}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ExtensionManagerImpl implements ExtensionManager {

  //private static final Logger log = LoggerFactory.getLogger(ExtensionManagerImpl.class);
  private BundleContext m_BundleContext;
  private Map<String, ServerExtension> m_Extensions;
  private ExtensionListener m_ExtensionListener;

  public ExtensionManagerImpl() {
    m_Extensions = new HashMap<String, ServerExtension>();
  }//ExtensionManagerImpl

  /**
   * Activates this <tt>ExtensionManagerImpl</tt>.
   * The logic will automatically register all {@link net.coalevo.empp.server.model.ServerExtension} class
   * objects, whether registered before or after the activation (i.e. white board
   * model implementation).
   *
   * @param bc the <tt>BundleContext</tt>.
   */
  public void activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    //prepare listener
    m_ExtensionListener = new ExtensionListener();
    //prepare the filter
    String filter = "(objectclass=" + ServerExtension.class.getName() + ")";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(m_ExtensionListener, filter);
      //ensure that already registered Provider instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        m_ExtensionListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      Activator.log().error("activate()", ex);
    }
  }//activate

  /**
   * Deactivates this <tt>ShellCommandProviderManagerImpl</tt>.
   * The logic will remove the listener and release all
   * references.
   */
  public void deactivate() {
    //remove the listener
    m_BundleContext.removeServiceListener(m_ExtensionListener);
    //null out the references
    unregisterAll();
    m_Extensions = null;
    m_ExtensionListener = null;
    m_BundleContext = null;
  }//deactivate

  public boolean register(ServerExtension ext) {
    final String ns = ext.getNamespace();
    if (m_Extensions.containsKey(ns)) {
      return false;
    } else {
      //keep reference
      m_Extensions.put(ns, ext);
      if (ext instanceof ServerPresenceExtension) {
        //register namespace extension and handler
        Stanzas.registerPresenceExtension(
            ns,
            ext.getNamespaceExtension()
        );
        if(ext.providesHandler()) {
          Activator.getPacketHandler().getPresenceHandler().registerHandler(
            ns,
            ext.getExtensionHandler()
          );
        }
      } else if (ext instanceof ServerIQExtension) {
        Stanzas.registerIQExtension(
            ns,
            ext.getNamespaceExtension()
        );
        if(ext.providesHandler()) {
          Activator.getPacketHandler().getIQHandler().registerHandler(
            ns,
            ext.getExtensionHandler()
          );
        }
      } else if (ext instanceof ServerInstantMessageExtension) {
        Stanzas.registerIMExtension(
            ns,
            ext.getNamespaceExtension()
        );
        if(ext.providesHandler()) {
          Activator.getPacketHandler().getMessageHandler().registerHandler(
            ns,
            ext.getExtensionHandler()
          );
        }
      }
      Activator.log().info("Registered ServerExtension " + ns);
      return true;
    }
  }//register

  public boolean unregister(String ns) {
    if (!m_Extensions.containsKey(ns)) {
      return false;
    } else {

      ServerExtension ext = m_Extensions.remove(ns);
      if (ext instanceof ServerPresenceExtension) {
        //register namespace extension and handler
        Stanzas.unregisterPresenceExtension(ns);
        Activator.getPacketHandler().getPresenceHandler().unregisterHandler(ns);
      } else if (ext instanceof ServerIQExtension) {
        Stanzas.unregisterIQExtension(ns);
        Activator.getPacketHandler().getIQHandler().unregisterHandler(ns);
      } else if (ext instanceof ServerInstantMessageExtension) {
        Stanzas.unregisterIMExtension(ns);
        Activator.getPacketHandler().getMessageHandler().unregisterHandler(ns);
      }

      Activator.log().info("Unregistered Extension " + ns);
      return true;
    }
  }//unregister

  private void unregisterAll() {
    for (String namespace : m_Extensions.keySet()) {
      unregister(namespace);
    }
  }//unregisterAll

  public ServerExtension get(String ns) {
    Object o = m_Extensions.get(ns);
    if (o != null) {
      return (ServerExtension) o;
    } else {
      throw new NoSuchElementException(ns);
    }
  }//get

  public boolean isAvailable(String namespace) {
    return m_Extensions.containsKey(namespace);
  }//isAvailable

  public Iterator listAvailable() {
    return m_Extensions.keySet().iterator();
  }//listAvailable

  private class ExtensionListener
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            Activator.log().error("ServiceListener:serviceChanged:registered:null");
          } else if (!(o instanceof ServerExtension)) {
            Activator.log().error("ServiceListener:serviceChanged:registered:Reference not a ServerExtension instance.");
          } else {
            register((ServerExtension) o);
          }
          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            Activator.log().error("ServiceListener:serviceChanged:unregistering:null");
          } else if (!(o instanceof ServerExtension)) {
            Activator.log().error("ServiceListener:serviceChanged:unregistering:Reference not a ServerExtension instance.");
          } else {
            unregister(((ServerExtension) o).getNamespace());
          }
          break;
      }
    }
  }//inner class ExtensionListenerListener

}//class ExtensionManagerImpl
