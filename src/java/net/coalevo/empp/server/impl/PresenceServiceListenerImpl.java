/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.impl;

import net.coalevo.empp.impl.*;
import net.coalevo.empp.server.ext.NicknameExtension;
import net.coalevo.empp.server.ext.NicknameNamespaceExtension;
import net.coalevo.empp.server.ext.RolesExtension;
import net.coalevo.empp.server.ext.RolesNamespaceExtension;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.presence.model.*;
import net.coalevo.presence.model.PresenceStatusTypes;
import net.coalevo.security.service.SecurityService;
import net.coalevo.userdata.service.UserdataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.mina.common.IoSession;

import java.util.List;

/**
 * Provides a {@link PresenceServiceListener} implementation
 * that will forward the handled events via EMPP to the
 * remote client.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class PresenceServiceListenerImpl
    implements PresenceServiceListener {

  //private static final Logger log = LoggerFactory.getLogger(PresenceServiceListenerImpl.class);
  private IoSession m_IOSession;
  private Presence m_Owner;
  private String m_SessionIdentifier;

  private ServiceMediator m_Services = Activator.getServices();

  public PresenceServiceListenerImpl(IoSession ios, String sesid) {
    m_IOSession = ios;
    m_SessionIdentifier = sesid;
  }//constructor

  public Presence getOwner() {
    return m_Owner;
  }//getOwner

  public void setOwner(Presence owner) {
    if (m_Owner == null) {
      m_Owner = owner;
    }
  }//setOwner

  public void found(PresenceProxy proxy) {
    statusUpdated(proxy);
  }//found

  public void receivedPresence(PresenceProxy proxy) {
    statusUpdated(proxy);
  }//receivedPresence

  public void subscriptionAllowed(AgentIdentifier rid) {
    try {
      net.coalevo.empp.model.Presence p =
          (net.coalevo.empp.model.Presence) Packets.leasePacket(PacketKinds.PRESENCE);
      p.setSessionIdentifier(m_SessionIdentifier);
      p.setFrom(rid);
      p.setTo(m_Owner.getAgentIdentifier());
      p.setType(PresenceTypes.SUBSCRIPTION);
      p.setSubscriptionAction(SubscriptionActions.ALLOW);
      p.preparePayload();
      m_IOSession.write(p);
    } catch (Exception ex) {
      Activator.log().error("subscriptionAllowed()", ex);
    }
  }//subscriptionAllowed

  public void subscriptionDenied(AgentIdentifier rid, String reason) {
    try {
      net.coalevo.empp.model.Presence p =
          (net.coalevo.empp.model.Presence) Packets.leasePacket(PacketKinds.PRESENCE);
      p.setSessionIdentifier(m_SessionIdentifier);
      p.setFrom(rid);
      p.setTo(m_Owner.getAgentIdentifier());
      p.setType(PresenceTypes.SUBSCRIPTION);
      p.setSubscriptionAction(SubscriptionActions.DENY);
      if (reason != null && reason.length() > 0) {
        p.setReason(reason);
      }
      p.preparePayload();
      m_IOSession.write(p);
    } catch (Exception ex) {
      Activator.log().error("subscriptionDenied()", ex);
    }
  }//subscriptionDenied

  public void subscriptionCanceled(AgentIdentifier rid) {
    try {
      net.coalevo.empp.model.Presence p =
          (net.coalevo.empp.model.Presence) Packets.leasePacket(PacketKinds.PRESENCE);
      p.setSessionIdentifier(m_SessionIdentifier);
      p.setFrom(rid);
      p.setTo(m_Owner.getAgentIdentifier());
      p.setType(PresenceTypes.SUBSCRIPTION);
      p.setSubscriptionAction(SubscriptionActions.CANCEL);
      p.preparePayload();
      m_IOSession.write(p);
    } catch (Exception ex) {
      Activator.log().error("subscriptionCanceled()", ex);
    }
  }//subscriptionCanceled

  public void unsubscribed(AgentIdentifier rid) {
    try {
      net.coalevo.empp.model.Presence p =
          (net.coalevo.empp.model.Presence) Packets.leasePacket(PacketKinds.PRESENCE);
      p.setSessionIdentifier(m_SessionIdentifier);
      p.setFrom(rid);
      p.setTo(m_Owner.getAgentIdentifier());
      p.setType(PresenceTypes.SUBSCRIPTION);
      p.setSubscriptionAction(SubscriptionActions.UNSUBSCRIBE);
      p.preparePayload();
      m_IOSession.write(p);
    } catch (Exception ex) {
      Activator.log().error("unsubscribed()", ex);
    }
  }//unsubscribed

  public void becamePresent(PresenceProxy proxy) {
    try {
      net.coalevo.empp.model.Presence p =
          (net.coalevo.empp.model.Presence) Packets.leasePacket(PacketKinds.PRESENCE);
      p.setSessionIdentifier(m_SessionIdentifier);
      p.setFrom(proxy.getAgentIdentifier());
      p.setTo(m_Owner.getAgentIdentifier());
      p.setType(PresenceTypes.PRESENT);
      p.setStatusType(translateType(proxy.getStatusType()));
      p.setDescription(proxy.getStatusDescription());
      p.setResources(getResources(proxy));
      addExtensions(p, proxy);

      p.preparePayload();
      m_IOSession.write(p);
    } catch (Exception ex) {
      Activator.log().error("becamePresent()", ex);
    }
  }//becamePresent

  public void becameAbsent(PresenceProxy proxy) {
    try {
      net.coalevo.empp.model.Presence p =
          (net.coalevo.empp.model.Presence) Packets.leasePacket(PacketKinds.PRESENCE);
      p.setSessionIdentifier(m_SessionIdentifier);
      p.setFrom(proxy.getAgentIdentifier());
      p.setTo(m_Owner.getAgentIdentifier());
      p.setType(PresenceTypes.ABSENT);
      p.preparePayload();
      m_IOSession.write(p);
    } catch (Exception ex) {
      Activator.log().error("becamePresent()", ex);
    }
  }//becameAbsent

  public void statusUpdated(PresenceProxy proxy) {
    try {
      System.err.println("statusUpdated()" + Thread.currentThread().toString());
      net.coalevo.empp.model.Presence p =
          (net.coalevo.empp.model.Presence)
              Packets.leasePacket(PacketKinds.PRESENCE);
      p.setSessionIdentifier(m_SessionIdentifier);
      p.setFrom(proxy.getAgentIdentifier());
      p.setTo(m_Owner.getAgentIdentifier());
      p.setType(PresenceTypes.STATUSUPDATE);
      p.setResources(getResources(proxy));
      p.setStatusType(translateType(proxy.getStatusType()));
      p.setDescription(proxy.getStatusDescription());
      addExtensions(p, proxy);

      p.preparePayload();
      m_IOSession.write(p);
    } catch (Exception ex) {
      Activator.log().error("statusUpdated()", ex);
    }
  }//statusUpdated

  private void addExtensions(net.coalevo.empp.model.Presence p, PresenceProxy proxy) {
    //TODO: Mechanism should be dynamic, e.g. any registered extensions should be added ?.
    if (proxy.isUser()) {
      //System.err.println("Proxy is user.");
      try {
        //Nickname info extension
        if (Stanzas.existsPresenceExtension(NicknameExtension.NAMESPACE)) {
          NicknameNamespaceExtension next = (NicknameNamespaceExtension)
              Stanzas.leasePresenceExtension(NicknameExtension.NAMESPACE);
          UserdataService uds = m_Services.getUserdataService(ServiceMediator.NO_WAIT);
          if (uds != null) {
            next.setNickname(
                uds.getNickname(m_Services.getServiceAgent(), proxy.getAgentIdentifier())
            );
            p.addExtension(NicknameExtension.NAMESPACE, next);
            //System.err.println("Added extension. " + next.toString());
          }
        }
      } catch (Exception ex) {
        //log but dont add
        Activator.log().error("addExtensions()", ex);
      }
    }
    //Role info extension
    try {
      if (Stanzas.existsPresenceExtension(RolesExtension.NAMESPACE)) {
        RolesNamespaceExtension rext = (RolesNamespaceExtension)
            Stanzas.leasePresenceExtension(RolesExtension.NAMESPACE);
        SecurityService sser = m_Services.getSecurityService(ServiceMediator.NO_WAIT);
        if (sser != null) {
          rext.setRoleNames(sser.getAgentRolesByName(
              m_Services.getServiceAgent(),
              proxy.getAgentIdentifier()
          )
          );
          p.addExtension(RolesExtension.NAMESPACE, rext);
          //System.err.println("Added extension. " + rext.toString());
        }
      }
    } catch (Exception ex) {
      //log but dont add
      Activator.log().error("addExtensions()", ex);
    }
  }//addExtensions

  public void resourcesUpdated(PresenceProxy proxy) {
    try {
      net.coalevo.empp.model.Presence p =
          (net.coalevo.empp.model.Presence) Packets.leasePacket(PacketKinds.PRESENCE);
      p.setSessionIdentifier(m_SessionIdentifier);
      p.setFrom(proxy.getAgentIdentifier());
      p.setTo(m_Owner.getAgentIdentifier());
      p.setType(PresenceTypes.RESOURCEUPDATE);
      p.setResources(getResources(proxy));
      p.preparePayload();
      m_IOSession.write(p);
    } catch (Exception ex) {
      Activator.log().error("resourcesUpdated()", ex);
    }
  }//resourcesUpdated

  public void requestedSubscription(PresenceProxy proxy) {
    try {
      net.coalevo.empp.model.Presence p =
          (net.coalevo.empp.model.Presence) Packets.leasePacket(PacketKinds.PRESENCE);
      p.setSessionIdentifier(m_SessionIdentifier);
      p.setFrom(proxy.getAgentIdentifier());
      p.setTo(m_Owner.getAgentIdentifier());
      p.setType(PresenceTypes.SUBSCRIPTION);
      p.setSubscriptionAction(SubscriptionActions.REQUEST);
      p.preparePayload();
      m_IOSession.write(p);
    } catch (Exception ex) {
      Activator.log().error("requestedSubscription()", ex);
    }
  }//requestedSubscription

  public boolean isActive() {
    return true;
  }//isActive

  private net.coalevo.empp.model.PresenceStatusType translateType(PresenceStatusType ptype) {
    if (PresenceStatusTypes.Available.equals(ptype)) {
      return net.coalevo.empp.impl.PresenceStatusTypes.AVAILABLE;
    } else if (PresenceStatusTypes.AdvertisedAvailable.equals(ptype)) {
      return net.coalevo.empp.impl.PresenceStatusTypes.ADVERTISED_AVAILABLE;
    } else if (PresenceStatusTypes.Unavailable.equals(ptype)) {
      return net.coalevo.empp.impl.PresenceStatusTypes.UNAVAILABLE;
    } else if (PresenceStatusTypes.TemporarilyUnavailable.equals(ptype)) {
      return net.coalevo.empp.impl.PresenceStatusTypes.TEMPORARILY_UNAVAILABLE;
    } else if (PresenceStatusTypes.AdvertisedTemporarilyUnavailable.equals(ptype)) {
      return net.coalevo.empp.impl.PresenceStatusTypes.ADVERTISED_TEMPORARILY_UNAVAILABLE;
    } else {
      return null;
    }
  }//translateType

  private String[] getResources(PresenceProxy proxy) {
    List resources = proxy.listResources();
    String[] res = new String[resources.size()];
    return ((String[]) resources.toArray(res));
  }//getResources

}//class PresenceServiceListenerImpl
