/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.impl;

import net.coalevo.empp.io.*;
import net.coalevo.empp.model.*;
import net.coalevo.foundation.model.AgentIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.mina.common.IoFilter;
import org.apache.mina.common.IoHandlerAdapter;
import org.apache.mina.common.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.LoggingFilter;

/**
 * Provides a handler for {@link EMPPPacket} instances received
 * from the network.
 * <p>
 * This implementation will delegate the packets to the corresponding
 * handlers. Basic checks (active session, authenticated, from) will
 * be excecuted here.
 * </p>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class PacketHandler
    extends IoHandlerAdapter {

  //private static final Logger log = LoggerFactory.getLogger(PacketHandler.class);

  private IoFilter LOGGING_FILTER = new LoggingFilter();
  private IoFilter CODEC_FILTER = new ProtocolCodecFilter(new EMPPCodecFactory());
  private IoFilter TS_FILTER = new TSFilter();
  private SessionHandler m_SessionHandler;
  private PresenceHandler m_PresenceHandler;
  private MessageHandler m_MessageHandler;
  private IQHandler m_IQHandler;

  static final AgentIdentifier from = new AgentIdentifier("");

  public void prepare() {
    m_SessionHandler = new SessionHandler();
    m_PresenceHandler = new PresenceHandler();
    m_MessageHandler = new MessageHandler();
    m_IQHandler = new IQHandler();
  }//prepare

  public PresenceHandler getPresenceHandler() {
    return m_PresenceHandler;
  }//getPresenceHandler

  public MessageHandler getMessageHandler() {
    return m_MessageHandler;
  }//getMessageHandler

  public IQHandler getIQHandler() {
    return m_IQHandler;
  }//getIQHandler

  public void sessionCreated(IoSession session) throws Exception {
    session.getFilterChain().addLast("logger", LOGGING_FILTER);
    session.getFilterChain().addLast("security", TS_FILTER);
    session.getFilterChain().addLast("codec", CODEC_FILTER);
    //prepare attributes
    session.setAttribute(EMPPEncoder.OUTPUT_BUFFER, new ByteBufferOutput());
    session.setAttribute(EMPPDecoder.INPUT_BUFFER, new ByteBufferInput());
  }//sessionCreated

  public void exceptionCaught(IoSession session, Throwable cause) {
    cause.printStackTrace();
    // Close connection when unexpected exception is caught.
    session.close();
  }//exceptionCaught

  public void messageReceived(IoSession ios, Object message) {
    try {
      if (message instanceof Session) {
        Session s = (Session) message;
        s.parsePayload();
        m_SessionHandler.handleSessionPacket(ios, s);
      } else {
        EMPPPacket packet = (EMPPPacket) message;
        NetworkSessionImpl ns = m_SessionHandler.getSession(packet.getSessionIdentifier());
        ns.activity();
        if (ns != null && ns.isAuthenticated()
            && ns.getAgentIdentifier().equals(packet.getFrom())) {
          packet.parsePayload();
          if (message instanceof Presence) {
            m_PresenceHandler.handlePresencePacket(ios, ns, (Presence)packet);
          } else if (message instanceof InstantMessage) {
            m_MessageHandler.handleMessagePacket(ios, ns, (InstantMessage) packet);
          } else if (message instanceof IQ) {
            m_MessageHandler.handleMessagePacket(ios, ns, (IQ) packet);
          } else {
            Activator.log().error("Unknown packet kind.");
          }
        } else {
          ios.close();
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }//messageReceived

  public void sessionClosed(IoSession session)
      throws Exception {
    Object o = session.getAttribute(SessionHandler.EMPP_SESSION);
    if(o != null) {
      m_SessionHandler.sessionClosed((String) o);
    }
    Activator.log().info("sessionClosed()" + session.toString());
  }//sessionClosed

}//class PacketHandler
