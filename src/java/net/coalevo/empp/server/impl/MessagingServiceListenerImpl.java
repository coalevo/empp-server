/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.impl;

import net.coalevo.empp.impl.*;
import net.coalevo.empp.model.InstantMessageType;
import net.coalevo.empp.model.Language;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.messaging.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.mina.common.IoSession;

import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/**
 * Provides a {@link MessagingServiceListener} implementation
 * that will forward the handled events via EMPP to the
 * remote client.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MessagingServiceListenerImpl
    implements MessagingServiceListener {

  //private static final Logger log = LoggerFactory.getLogger(MessagingServiceListenerImpl.class);
  private IoSession m_IOSession;
  private AgentIdentifier m_Owner;
  private String m_SessionIdentifier;

  public MessagingServiceListenerImpl(IoSession ios, AgentIdentifier aid, String sesid) {
    m_IOSession = ios;
    m_Owner = aid;
    m_SessionIdentifier = sesid;
  }//constructor

  public boolean received(InteractiveMessage msg) {
    try {
      net.coalevo.empp.model.InstantMessage m =
          (net.coalevo.empp.model.InstantMessage) Packets.leasePacket(PacketKinds.INSTANT_MESSAGE);

      //copy over
      m.setSessionIdentifier(m_SessionIdentifier);
      m.setIdentifier(msg.getIdentifier());
      m.setFrom(msg.getFrom());
      m.setTo(msg.getTo());
      m.setType(translateType(msg.getType()));
      m.setThread(msg.getThread());
      m.setFormattingHint(msg.getFormattingHint());
      if (msg.hasSubjects()) {
        for (Iterator iterator = msg.getSubjects(); iterator.hasNext();) {
          Map.Entry entry = (Map.Entry) iterator.next();
          m.setSubject(toLanguage((Locale) entry.getKey()), (String) entry.getValue());
        }
      }
      msg.getFormattingHint();
      if (msg.hasBodies()) {
        for (Iterator iterator = msg.getBodies(); iterator.hasNext();) {
          Map.Entry entry = (Map.Entry) iterator.next();
          m.setBody(toLanguage((Locale) entry.getKey()), (String) entry.getValue());
        }
      }
      m.setConfirmationRequired(msg.isConfirmationRequired());
      m.preparePayload();
      m_IOSession.write(m);
      return true;
    } catch (Exception ex) {
      Activator.log().error("received()", ex);
    }
    return false;
  }//received


  public boolean received(Request r) {
    //TODO: need to make it an IQ :) ho ho ho :)
    return false;  //To change body of implemented methods use File | Settings | File Templates.
  }

  public boolean received(Response r) {
    //TODO: need to make it an IQ :) ho ho :)
    return false;  //To change body of implemented methods use File | Settings | File Templates.
  }

  public void confirmed(EventInfo ei, long timestamp) {
    try {
      net.coalevo.empp.model.InstantMessage m =
          (net.coalevo.empp.model.InstantMessage) Packets.leasePacket(PacketKinds.INSTANT_MESSAGE);
      //copy over
      m.setSessionIdentifier(m_SessionIdentifier);
      m.setIdentifier(ei.getIdentifier());
      m.setFrom(ei.getTo());
      m.setTo(m_Owner);
      m.setType(InstantMessageTypes.CONFIRMATION);
      m.setConfirmationRequired(false);
      m.setReceptionTime(timestamp);
      m.preparePayload();
      m_IOSession.write(m);
    } catch (Exception ex) {
      Activator.log().error("confirmed()", ex);
    }
  }//confirmed

  public void failedDelivery(Message m) {

    //TODO: Should be an error
    //To change body of implemented methods use File | Settings | File Templates.

  }

  public void startedMessage(AgentIdentifier from, String msgid) {
    try {
      net.coalevo.empp.model.InstantMessage m =
          (net.coalevo.empp.model.InstantMessage) Packets.leasePacket(PacketKinds.INSTANT_MESSAGE);
      //copy over
      System.err.println((from ==null)?"FROM NULL":"FROM NOT NULL");
      System.err.println((m_SessionIdentifier ==null)?"SESID NULL":"SESID NOT NULL");
      System.err.println((m_Owner==null)?"OWNER NULL":"OWNER NOT NULL");

      m.setSessionIdentifier(m_SessionIdentifier);
      m.setIdentifier(msgid);
      m.setFrom(from);
      m.setTo(m_Owner);
      m.setType(InstantMessageTypes.EVENT);
      m.setEventType(MessageEventTypes.MSG_START);
      m.preparePayload();
      m_IOSession.write(m);
    } catch (Exception ex) {
      Activator.log().error("startedMessage()", ex);
    }
  }//startedMessage

  public void stoppedMessage(AgentIdentifier from, String msgid) {
    try {
      net.coalevo.empp.model.InstantMessage m =
          (net.coalevo.empp.model.InstantMessage) Packets.leasePacket(PacketKinds.INSTANT_MESSAGE);
      //copy over
      m.setSessionIdentifier(m_SessionIdentifier);
      m.setIdentifier(msgid);
      m.setFrom(from);
      m.setTo(m_Owner);
      m.setType(InstantMessageTypes.EVENT);
      m.setEventType(MessageEventTypes.MSG_STOP);
      m.preparePayload();
      m_IOSession.write(m);
    } catch (Exception ex) {
      Activator.log().error("stoppedMessage()", ex);
    }
  }//stoppedMessage

  private InstantMessageType translateType(InteractiveMessageType type) {
    if (InteractiveMessageTypes.CHAT.equals(type)) {
      return InstantMessageTypes.CHAT;
    } else if (InteractiveMessageTypes.GROUPCHAT.equals(type)) {
      return InstantMessageTypes.GROUPCHAT;
    } else if (InteractiveMessageTypes.NEWS.equals(type)) {
      return InstantMessageTypes.NEWS;
    } else if (InteractiveMessageTypes.NOTIFICATION.equals(type)) {
      return InstantMessageTypes.NOTIFICATION;
    }
    return InstantMessageTypes.CHAT;
  }//translateType

  private Language toLanguage(Locale l) {
    System.err.println("Locale LANGUAGE: "  + l.getLanguage());
    return Languages.getLanguage(l.getLanguage());
  }//toLanguage

}//class MessagingServiceListenerImpl
