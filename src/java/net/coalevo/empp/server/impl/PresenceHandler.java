/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.impl;

import net.coalevo.empp.impl.*;
import net.coalevo.empp.model.*;
import net.coalevo.presence.model.PresenceStatus;
import net.coalevo.presence.model.AgentIdentifierList;
import net.coalevo.presence.service.PresenceService;
import net.coalevo.userdata.service.UserdataService;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Agent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.mina.common.IoSession;

import java.util.Iterator;

/**
 * Provides the handler for {@link Presence} packets.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class PresenceHandler
    extends ExtensiblePacketHandler {

  //private static final Logger log = LoggerFactory.getLogger(PresenceHandler.class);
  private ServiceMediator m_Services = Activator.getServices();

  public PresenceHandler() {
  }//PresenceHandler

  public void handlePresencePacket(IoSession io, NetworkSessionImpl ns, Presence p) {
    try {
      PresenceType pt = (PresenceType) p.getType();
      net.coalevo.presence.model.Presence pres = ns.getPresence();
      if (PresenceTypes.PRESENT.equals(pt)) {
        if (p.hasStatusType()) {
          PresenceStatus pst = pres.getStatus();
          pst.setType(translateType(p.getStatusType()));
          //System.err.println("Set type.");
          if (p.hasDescription()) {
            pst.setDescription(p.getDescription());
            //System.err.println("Set description.");
          }
          if (p.hasResources()) {
            pres.setResources(p.getResources());
            //System.err.println("Set resources.");
          }
          pres.begin(pst);
          //System.err.println("Began presence.");
        } else {
          pres.begin();
        }
      } else if (PresenceTypes.ABSENT.equals(pt)) {
        pres.end();
        //TODO: two things, 1. does the session end here, 2. we need to make sure
        // that the absence is given when the session is ended.
      } else if (PresenceTypes.STATUSUPDATE.equals(pt)) {
        PresenceStatus pst = pres.getStatus();
        if (p.hasStatusType() && !pst.isType(translateType(p.getStatusType()))) {
          pst.setType(translateType(p.getStatusType()));
          if (p.hasDescription()) {
            pst.setDescription(p.getDescription());
          }
          if (p.hasResources()) {
            pres.setResources(p.getResources());
          }
          pres.updateStatus(pst);
        }
      } else if (PresenceTypes.RESOURCEUPDATE.equals(pt)) {
        if (p.hasResources()) {
          pres.updateResources(p.getResources());
        }
      } else if (PresenceTypes.PROBE.equals(pt)) {
        pres.getService().probePresence(pres, p.getTo());
      } else if(PresenceTypes.SUBSCRIPTION.equals(pt) && p.hasSubscriptionAction()) {
        //handle per action type
        SubscriptionAction sa = p.getSubscriptionAction();
        PresenceService ps = m_Services.getPresenceService(ServiceMediator.NO_WAIT);
        if(SubscriptionActions.REQUEST.equals(sa)) {
          ps.requestSubscriptionTo(pres,p.getTo());
        } else if (SubscriptionActions.ALLOW.equals(sa)) {
          ps.allowSubscription(pres,p.getTo());
        } else if (SubscriptionActions.DENY.equals(sa)) {
          ps.denySubscription(pres,p.getTo(),p.getReason());
        } else if (SubscriptionActions.CANCEL.equals(sa)) {
          ps.cancelSubscriptionOf(pres,p.getTo(),true);
        } else if (SubscriptionActions.UNSUBSCRIBE.equals(sa)) {
          ps.unsubscribeFrom(pres,p.getTo(),true);
        } else if(SubscriptionActions.isListAction(sa)) {
          UserdataService ud = m_Services.getUserdataService(ServiceMediator.NO_WAIT);
          Agent a = pres.getAgent();
          boolean limit = false;
          net.coalevo.empp.model.Presence pr =
              (net.coalevo.empp.model.Presence) Packets.leasePacket(PacketKinds.PRESENCE);

          //Set Session Identifier
          pr.setSessionIdentifier(ns.getSessionIdentifier());
          pr.setFrom(m_Services.getServiceAgent().getAgentIdentifier());
          pr.setTo(p.getFrom());
          pr.setType(PresenceTypes.SUBSCRIPTION);
          Iterator<AgentIdentifier> agents = null;
          if (SubscriptionActions.LIST_FROM.equals(sa)) {
            //requires reply
            AgentIdentifierList l = pres.getSubscriptionsFrom();
            agents = l.iterator();
            pr.setSubscriptionAction(SubscriptionActions.LIST_FROM);
          } else if (SubscriptionActions.LIST_TO.equals(sa)) {
            AgentIdentifierList l = pres.getSubscriptionsTo();
            agents = l.iterator();
            pr.setSubscriptionAction(SubscriptionActions.LIST_TO);
          } else if (SubscriptionActions.LIST_REQ.equals(sa)) {
            AgentIdentifierList l = pres.getRequestedSubscriptions();
            agents = l.iterator();
            pr.setSubscriptionAction(SubscriptionActions.LIST_REQ);
          } else if (SubscriptionActions.LIST_AD.equals(sa)) {
            //TODO: Transmit filter?
            agents = ps.listAdvertisedPresent(a,null);
            pr.setSubscriptionAction(SubscriptionActions.LIST_AD);            
          }
          if(agents!=null) {
            while(agents.hasNext()) {
              AgentIdentifier aid = agents.next();
              String str = null;
              Activator.log().debug("===>" + aid.getIdentifier());
              if(aid.getIdentifier().contains(".service.")) {
                str = "Service";
              } else {
                str = ud.getNickname(a,aid);
              }
              pr.addItem(aid,str);
              if(limit && pr.getItemCount() > 100) {
                break;
              }
            }
          }
          pr.preparePayload();
          io.write(pr);
        } else {
           Activator.log().error("handlePresencePacket()::Unimplemented subscription action type.");
           return;
        }
      } else if(PresenceTypes.PRIVACY.equals(pt) && p.hasPrivacyAction()) {
        //handle per action type
        PresencePrivacyAction ppa = p.getPrivacyAction();
        PresenceService ps = m_Services.getPresenceService(ServiceMediator.NO_WAIT);
        if(PresencePrivacyActions.BLOCK_INBOUND.equals(ppa)) {
          ps.blockInbound(pres,p.getTo());
        } else if (PresencePrivacyActions.BLOCK_OUTBOUND.equals(ppa)) {
          ps.blockOutbound(pres,p.getTo());
        } else if (PresencePrivacyActions.UNBLOCK_INBOUND.equals(ppa)) {
          ps.allowInbound(pres,p.getTo());
        } else if (PresencePrivacyActions.UNBLOCK_OUTBOUND.equals(ppa)) {
          ps.allowOutbound(pres,p.getTo());
        } else if(PresencePrivacyActions.isListAction(ppa)) {
          UserdataService ud = m_Services.getUserdataService(ServiceMediator.NO_WAIT);
          Agent a = pres.getAgent();
          boolean limit = false;
          net.coalevo.empp.model.Presence pr =
              (net.coalevo.empp.model.Presence) Packets.leasePacket(PacketKinds.PRESENCE);

          //Set Session Identifier
          pr.setSessionIdentifier(ns.getSessionIdentifier());
          pr.setFrom(m_Services.getServiceAgent().getAgentIdentifier());
          pr.setTo(p.getFrom());
          pr.setFrom(Activator.getServerIdentifier());
          pr.setType(PresenceTypes.PRIVACY);
          Iterator<AgentIdentifier> agents = null;
          if (PresencePrivacyActions.LIST_INBOUND_BLOCKED.equals(ppa)) {
            //requires reply
            AgentIdentifierList l = pres.getInboundBlocked();
            agents = l.iterator();
            pr.setPrivacyAction(PresencePrivacyActions.LIST_INBOUND_BLOCKED);

          } else if (PresencePrivacyActions.LIST_INBOUND_BLOCKED.equals(ppa)) {
            //requires reply
            AgentIdentifierList l = pres.getOutboundBlocked();
            agents = l.iterator();
            pr.setPrivacyAction(PresencePrivacyActions.LIST_OUTBOUND_BLOCKED);
          }
          if(agents!=null) {
            while(agents.hasNext()) {
              AgentIdentifier aid = agents.next();
              String str = ud.getNickname(a,aid);
              pr.addItem(aid,str);
              if(limit && pr.getItemCount() > 100) {
                break;
              }
            }
          }
          pr.preparePayload();
          io.write(pr);
        } else {
           Activator.log().error("handlePresencePacket()::Unimplemented subscription action type.");
           return;
        }
      } else {
        Activator.log().error("handlePresencePacket()::Unimplemented type.");
        return;
      }
      //TODO: could there be presence extensions that require response?
      if(p.hasExtensions()) {
        handleExtensions(ns,p);
      }
    } catch (Exception ex) {
      Activator.log().error("handlePresencePacket()", ex);
    } finally {
      //release the leased package
      Packets.releasePacket(p);
    }
  }//handlePresencePacket

  private net.coalevo.presence.model.PresenceStatusType translateType(Type ptype) {
    if (PresenceStatusTypes.AVAILABLE.equals(ptype)) {
      return net.coalevo.presence.model.PresenceStatusTypes.Available;
    } else if (PresenceStatusTypes.ADVERTISED_AVAILABLE.equals(ptype)) {
      return net.coalevo.presence.model.PresenceStatusTypes.AdvertisedAvailable;
    } else if (PresenceStatusTypes.UNAVAILABLE.equals(ptype)) {
      return net.coalevo.presence.model.PresenceStatusTypes.Unavailable;
    } else if (PresenceStatusTypes.TEMPORARILY_UNAVAILABLE.equals(ptype)) {
      return net.coalevo.presence.model.PresenceStatusTypes.TemporarilyUnavailable;
    } else {
      return net.coalevo.presence.model.PresenceStatusTypes.Unavailable;
    }
  }//translateType


}//class PresenceHandler
