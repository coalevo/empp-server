/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.impl;

import net.coalevo.empp.server.service.EMPPServerConfiguration;
import net.coalevo.foundation.model.ActionIterator;
import net.coalevo.foundation.model.Service;
import net.coalevo.foundation.model.ServiceAgent;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.security.service.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.mina.common.IoAcceptor;
import org.apache.mina.transport.socket.nio.SocketAcceptor;
import org.osgi.framework.BundleContext;

import java.net.InetSocketAddress;
import java.util.Iterator;
import java.util.concurrent.Executors;

/**
 * Provides the EMPP server that will listen to
 * a specific socket.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
class EMPPServer implements Service, ConfigurationUpdateHandler {

  private IoAcceptor m_Acceptor;
  private int m_Port = 4242;
  private PacketHandler m_PacketHandler;

  public EMPPServer(int port) {
    m_Port = port;
  }//constructor

  public PacketHandler getPacketHandler() {
    return m_PacketHandler;
  }//getPacketHandler

  public void start() throws Exception {
    m_PacketHandler = new PacketHandler();
    m_PacketHandler.prepare();
    m_Acceptor = new SocketAcceptor(Runtime.getRuntime().availableProcessors() + 1, Executors.newCachedThreadPool());
    m_Acceptor.bind(
        new InetSocketAddress(m_Port),
        m_PacketHandler
    );
    Activator.log().info(Activator.getBundleMessages().get("EMPPServer.started", "port", "" + m_Port));
  }//start

  public void stop() throws Exception {
    if(m_Acceptor != null) {
      m_Acceptor.unbindAll();
      m_Acceptor = null;
    }
    Activator.log().info(Activator.getBundleMessages().get("EMPPServer.stopped"));
  }//stop

  public void update(MetaTypeDictionary mtd) {
    int port = 4242;
    try {
      port = mtd.getInteger(EMPPServerConfiguration.EMPP_SERVER_PORT_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(
          Activator.getBundleMessages().get("EMPPServer.configexception", "attribute", EMPPServerConfiguration.EMPP_SERVER_PORT_KEY),
          ex
      );
    }
    if (port != m_Port) {
      try {
        stop();
      } catch (Exception ex) {
        Activator.log().error(
            Activator.getBundleMessages().get("EMPPServer.exception"),
            ex
        );
      }
      m_Port = port;
      try {
        start();
      } catch (Exception ex) {
        Activator.log().error(
            Activator.getBundleMessages().get("EMPPServer.exception"),
            ex
        );
      }
    }
  }//update

  public boolean activate(BundleContext bc) {
    //authenticate
    SecurityService sser = Activator.getServices().getSecurityService(ServiceMediator.WAIT_UNLIMITED);
    if (sser != null) {
      Activator.getServices().setServiceAgent(sser.authenticate(this));
    }
    ServiceAgent sa = Activator.getServices().getServiceAgent();
    System.err.println((sa == null)? "SA NULL":sa.toString());
    return true;
  }//activate

  public boolean deactivate() {
    ServiceAgent sa = Activator.getServices().getServiceAgent();
    Activator.getServices().setServiceAgent(null);
    SecurityService sser = Activator.getServices().getSecurityService(ServiceMediator.NO_WAIT);
    System.err.println("Deactivate::sser "+ ((sser == null)? "sser null.":"sser not null."));

    if (sser != null) {
      sser.invalidateAuthentication(sa);
      System.err.println("Deactivat::invalidated "+ sa.toString());

    }
    return true;
  }//deactivate

  public Iterator getActions() {
    return ActionIterator.EMPTY_ITERATOR;
  }//getActions

  public String getIdentifier() {
    return EMPPServer.class.getName();
  }//getIdentifier

}//class EMPPServer
