/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.impl;

import net.coalevo.empp.impl.PacketKinds;
import net.coalevo.empp.impl.Packets;
import net.coalevo.empp.model.IQ;
import net.coalevo.empp.model.NamespaceExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.mina.common.IoSession;

/**
 * Implements an extensible handler for {@link IQ} packets.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class IQHandler extends ExtensiblePacketHandler {

  //private static final Logger log = LoggerFactory.getLogger(IQHandler.class);

  public void handleIQ(IoSession io, NetworkSessionImpl ns, IQ iq) {
    try {
      if (iq.hasExtensions()) {
        IQ resp = (IQ) Packets.leasePacket(PacketKinds.IQ);
        resp.setSessionIdentifier(ns.getSessionIdentifier());
        resp.setIdentifier(iq.getIdentifier());
        for (NamespaceExtension ext : handleExtensions(ns, iq)) {
          resp.addExtension(ext.getNamespace(), ext);
        }
        resp.preparePayload();
        io.write(resp);
      }
    } catch (Exception ex) {
      Activator.log().error("handleIQ()", ex);
    }
  }//handleIQ

}//class IQHandler
