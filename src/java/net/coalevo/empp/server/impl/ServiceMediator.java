/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.impl;

import net.coalevo.foundation.model.ServiceAgent;
import net.coalevo.foundation.service.MessageResourceService;
import net.coalevo.foundation.service.RndStringGeneratorService;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.security.service.PolicyService;
import net.coalevo.security.service.SecurityService;
import net.coalevo.security.service.SecurityManagementService;
import net.coalevo.system.service.ExecutionService;
import net.coalevo.system.service.SessionService;
import net.coalevo.userdata.service.UserdataService;
import net.coalevo.presence.service.PresenceService;
import net.coalevo.messaging.service.MessagingService;
import org.osgi.framework.*;
import javax.xml.parsers.SAXParserFactory;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


/**
 * Provides a mediator for required coalevo services.
 * Allows to obtain fresh and latest references by using
 * the whiteboard model to track the services at all times.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ServiceMediator  {

  private BundleContext m_BundleContext;
  private SecurityService m_SecurityService;
  private SecurityManagementService m_SecurityManagementService;
  private PolicyService m_PolicyService;
  private RndStringGeneratorService m_RndStringGeneratorService;
  private MessageResourceService m_MessageResourceService;
  private ExecutionService m_ExecutionService;

  //these are kind of "internal" services
  private UserdataService m_UserdataService;
  private PresenceService m_PresenceService;
  private MessagingService m_MessagingService;
  private SessionService m_SessionService;


  private CountDownLatch m_SecurityServiceLatch;
  private CountDownLatch m_SecurityManagementServiceLatch;
  private CountDownLatch m_PolicyServiceLatch;
  private CountDownLatch m_RndStringGeneratorLatch;
  private CountDownLatch m_MessageResourceServiceLatch;
  private CountDownLatch m_ExecutionServiceLatch;
  private CountDownLatch m_UserdataServiceLatch;
  private CountDownLatch m_PresenceServiceLatch;
  private CountDownLatch m_MessagingServiceLatch;
  private CountDownLatch m_SessionServiceLatch;

  private ConfigurationMediator m_ConfigurationMediator;

  private ServiceAgent m_ServiceAgent;

  public ServiceMediator() {

  }//constructor

  public void setServiceAgent(ServiceAgent a) {
    m_ServiceAgent = a;
  }//setServiceAgent

  public ServiceAgent getServiceAgent() {
    return m_ServiceAgent;
  }//getServiceAgent

  public SAXParserFactory getSAXParserFactory()
      throws Exception {
    ServiceReference refs[] = m_BundleContext.getServiceReferences(
        SAXParserFactory.class.getName(),
        "(&(parser.namespaceAware=true)"
            + "(parser.validating=true))");
    if (refs == null) {
      throw new Exception("Could not obtain SAX Parser Factory.");
    }
    return (SAXParserFactory) m_BundleContext.getService(refs[0]);
  }//getParserFactory

  public SecurityService getSecurityService(long wait) {
    try {
      if (wait < 0) {
        m_SecurityServiceLatch.await();
      } else if (wait > 0) {
        m_SecurityServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error("getSecurityService()", e);
    }
    return m_SecurityService;
  }//getSecurityService

  public SecurityManagementService getSecurityManagementService(long wait) {
    try {
      if (wait < 0) {
        m_SecurityManagementServiceLatch.await();
      } else if (wait > 0) {
        m_SecurityManagementServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error("getSecurityManagementService()", e);
    }
    return m_SecurityManagementService;
  }//getSecurityManagementService

  public PolicyService getPolicyService(long wait) {
    try {
      if (wait < 0) {
        m_PolicyServiceLatch.await();
      } else if (wait > 0) {
        m_PolicyServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error("getPolicyService()", e);
    }
    return m_PolicyService;
  }//getPolicyService

  public MessageResourceService getMessageResourceService(long wait) {
    try {
      if (wait < 0) {
        m_MessageResourceServiceLatch.await();
      } else if (wait > 0) {
        m_MessageResourceServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error("getMessageResourceService()", e);
    }
    return m_MessageResourceService;
  }//getMessageResourceService

  public RndStringGeneratorService getRndStringGeneratorService(long wait) {
    try {
      if (wait < 0) {
        m_RndStringGeneratorLatch.await();
      } else if (wait > 0) {
        m_RndStringGeneratorLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error("getRndStringGeneratorService()", e);
    }

    return m_RndStringGeneratorService;
  }//getRndStringGeneratorService

  public UserdataService getUserdataService(long wait) {
    try {
      if (wait < 0) {
        m_UserdataServiceLatch.await();
      } else if (wait > 0) {
        m_UserdataServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error("getUserdataService()", e);
    }

    return m_UserdataService;
  }//getUserdataService

  public PresenceService getPresenceService(long wait) {
    try {
      if (wait < 0) {
        m_PresenceServiceLatch.await();
      } else if (wait > 0) {
        m_PresenceServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error("getPresenceService()", e);
    }

    return m_PresenceService;
  }//getPresenceService

  public MessagingService getMessagingService(long wait) {
      try {
        if (wait < 0) {
          m_MessagingServiceLatch.await();
        } else if (wait > 0) {
          m_MessagingServiceLatch.await(wait, TimeUnit.MILLISECONDS);
        }
      } catch (InterruptedException e) {
        Activator.log().error("getMessagingService()", e);
      }

      return m_MessagingService;
    }//getMessagingService

  public ExecutionService getExecutionService(long wait) {
    try {
      if (wait < 0) {
        m_ExecutionServiceLatch.await();
      } else if (wait > 0) {
        m_ExecutionServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error("getExecutionService()", e);
    }

    return m_ExecutionService;
  }//getExecutionService

  public SessionService getSessionService(long wait) {
    try {
      if (wait < 0) {
        m_SessionServiceLatch.await();
      } else if (wait > 0) {
        m_SessionServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error("getExecutionService()", e);
    }

    return m_SessionService;
  }//getSessionService

  public ConfigurationMediator getConfigMediator() {
    return m_ConfigurationMediator;
  }//getConfigMediator

  public void setConfigMediator(ConfigurationMediator configMediator) {
    m_ConfigurationMediator = configMediator;
  }//setConfigMediator


  public boolean activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    //prepare waits if required
    m_SecurityServiceLatch = createWaitLatch();
    m_SecurityManagementServiceLatch = createWaitLatch();
    m_PolicyServiceLatch = createWaitLatch();
    m_RndStringGeneratorLatch = createWaitLatch();
    m_MessageResourceServiceLatch = createWaitLatch();
    m_ExecutionServiceLatch = createWaitLatch();
    m_UserdataServiceLatch = createWaitLatch();
    m_PresenceServiceLatch = createWaitLatch();
    m_MessagingServiceLatch = createWaitLatch();
    m_SessionServiceLatch = createWaitLatch();


    //prepareDefinitions listener
    ServiceListener serviceListener = new ServiceListenerImpl();

    //prepareDefinitions the filter
    String filter =
        "(|(|(|(|(|(|(|(|(|(objectclass=" + SecurityService.class.getName() + ")" +
            "(objectclass=" + SecurityManagementService.class.getName() + "))" +
            "(objectclass=" + PolicyService.class.getName() + "))" +
            "(objectclass=" + RndStringGeneratorService.class.getName() + "))" +
            "(objectclass=" + MessageResourceService.class.getName() + "))" +
            "(objectclass=" + ExecutionService.class.getName() + "))" +
            "(objectclass=" + UserdataService.class.getName() + "))" +
            "(objectclass=" + PresenceService.class.getName() + "))" +
            "(objectclass=" + MessagingService.class.getName() + "))" +
            "(objectclass=" + SessionService.class.getName() + "))" ;


    try {
      //add the listener to the bundle context.
      bc.addServiceListener(serviceListener, filter);

      //ensure that already registered Service instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        serviceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      Activator.log().error("activate()", ex);
      return false;
    }
    return true;
  }//activate

  public void deactivate() {
    //null out the references
    m_SecurityService = null;
    m_SecurityManagementService = null;
    m_PolicyService = null;
    m_MessageResourceService = null;
    m_UserdataService = null;
    m_ExecutionService = null;
    m_RndStringGeneratorService = null;
    m_PresenceService = null;
    m_MessagingService = null;
    m_SessionService = null;
    m_ConfigurationMediator = null;


    //Latches
    m_SecurityServiceLatch = null;
    m_SecurityManagementServiceLatch = null;
    m_PolicyServiceLatch = null;
    m_RndStringGeneratorLatch = null;
    m_MessageResourceServiceLatch = null;
    m_ExecutionServiceLatch = null;
    m_UserdataServiceLatch = null;
    m_PresenceServiceLatch = null;
    m_MessagingServiceLatch = null;
    m_SessionServiceLatch = null;
    m_BundleContext = null;
  }//deactivate

  private CountDownLatch createWaitLatch() {
    return new CountDownLatch(1);
  }//createWaitLatch

  private class ServiceListenerImpl
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      System.err.println(ev.toString());
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            Activator.log().error("ServiceListener:Registration:NULL");
            return;
          } else if (o instanceof SecurityService) {
            m_SecurityService = (SecurityService) o;
            m_SecurityServiceLatch.countDown();
            Activator.log().info("ServiceListener:Registration:SecurityService");
          } else if (o instanceof SecurityManagementService) {
            m_SecurityManagementService = (SecurityManagementService) o;
            m_SecurityManagementServiceLatch.countDown();
            Activator.log().info("ServiceListener:Registration:SecurityManagementService");
          } else if (o instanceof PolicyService) {
            m_PolicyService = (PolicyService) o;
            m_PolicyServiceLatch.countDown();
            Activator.log().info("ServiceListener:Registration:PolicyService");
          } else if (o instanceof RndStringGeneratorService) {
            m_RndStringGeneratorService = (RndStringGeneratorService) o;
            m_RndStringGeneratorLatch.countDown();
            Activator.log().info("ServiceListener:Registration:RndStringGeneratorService");
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = (MessageResourceService) o;
            m_MessageResourceServiceLatch.countDown();
            Activator.log().info("ServiceListener:Registration:MessageResourceService");
          } else if (o instanceof ExecutionService) {
            m_ExecutionService = (ExecutionService) o;
            m_ExecutionServiceLatch.countDown();
            Activator.log().info("ServiceListener:Registration:ExecutionService");
          } else if (o instanceof UserdataService) {
            m_UserdataService = (UserdataService) o;
            m_UserdataServiceLatch.countDown();
            Activator.log().info("ServiceListener:Registration:UserdataService");
          } else if (o instanceof PresenceService) {
            m_PresenceService = (PresenceService) o;
            m_PresenceServiceLatch.countDown();
            Activator.log().info("ServiceListener:Registration:PresenceService");
          } else if (o instanceof MessagingService) {
            m_MessagingService = (MessagingService) o;
            m_MessagingServiceLatch.countDown();
            Activator.log().info("ServiceListener:Registration:MessagingService");
          } else if (o instanceof SessionService) {
            m_SessionService = (SessionService) o;
            m_SessionServiceLatch.countDown();
            Activator.log().info("ServiceListener:Registration:SessionService");
          } else {
            m_BundleContext.ungetService(sr);
          }

          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            Activator.log().error("ServiceListener:Unregistration::null");
            return;
          } else if (o instanceof SecurityService) {
            m_SecurityService = null;
            m_SecurityServiceLatch = createWaitLatch();
            Activator.log().info("ServiceListener:Unregistration:SecurityService");
          } else if (o instanceof SecurityManagementService) {
            m_SecurityManagementService = null;
            m_SecurityManagementServiceLatch = createWaitLatch();
            Activator.log().info("ServiceListener:Unregistration:SecurityManagementService");
          } else if (o instanceof PolicyService) {
            m_PolicyService = null;
            m_PolicyServiceLatch = createWaitLatch();
            Activator.log().info("ServiceListener:Unregistration:PolicyService");
          } else if (o instanceof RndStringGeneratorService) {
            m_RndStringGeneratorService = null;
            m_RndStringGeneratorLatch = createWaitLatch();
            Activator.log().info("ServiceListener:Unregistration:RndStringGeneratorService");
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = null;
            m_MessageResourceServiceLatch = createWaitLatch();
            Activator.log().info("ServiceListener:Unregistration:MessageResourceService");
          } else if (o instanceof ExecutionService) {
            m_ExecutionService = null;
            m_ExecutionServiceLatch = createWaitLatch();
            Activator.log().info("ServiceListener:Unregistration:ExecutionService");
          } else if (o instanceof UserdataService) {
            m_UserdataService = null;
            m_UserdataServiceLatch = createWaitLatch();
            Activator.log().info("ServiceListener:Unregistration:UserdataService");
          } else if (o instanceof PresenceService) {
            m_PresenceService = null;
            m_PresenceServiceLatch = createWaitLatch();
            Activator.log().info("ServiceListener:Unregistration:PresenceService");
          } else if (o instanceof MessagingService) {
            m_MessagingService = null;
            m_MessagingServiceLatch = createWaitLatch();
            Activator.log().info("ServiceListener:Unregistration:MessagingService");
          } else if (o instanceof SessionService) {
            m_SessionService = null;
            m_SessionServiceLatch = createWaitLatch();
            Activator.log().info("ServiceListener:Unregistration:SessionService");
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
      }
    }
  }//inner class ServiceListenerImpl


  public static long WAIT_UNLIMITED = -1;
  public static long NO_WAIT = 0;

}//class ServiceMediator