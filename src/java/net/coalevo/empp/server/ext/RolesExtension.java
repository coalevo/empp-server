/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.ext;

import net.coalevo.empp.server.model.ServerPresenceExtension;
import net.coalevo.empp.server.model.NamespaceExtensionHandler;
import net.coalevo.empp.model.NamespaceExtension;

/**
 * Provides a {@link ServerPresenceExtension} that
 * will add roles information.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class RolesExtension
    implements ServerPresenceExtension {

  public String getNamespace() {
    return NAMESPACE;
  }//getNamespace

  public Class getNamespaceExtension() {
    return RolesNamespaceExtension.class;
  }//getNamespaceExtension

  public NamespaceExtensionHandler getExtensionHandler() {
    return null;
  }//getExtensionHandler

  public boolean providesHandler() {
    return false;
  }//providesHandler

  /**
   * Defines the namespace of this <tt>ServerExtension</tt>.
   */
  public static final String NAMESPACE = "net.coalevo.security.roles";

}//class RolesExtension
