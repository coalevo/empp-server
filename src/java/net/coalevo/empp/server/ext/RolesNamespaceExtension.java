/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.ext;

import net.coalevo.empp.model.*;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import java.util.Set;

/**
 * Provides the implementation of a simple {@link NamespaceExtension}
 * that is supposed to add role information to {@link Presence}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class RolesNamespaceExtension
    implements PresenceNamespaceExtension {

  private Set<String> m_RoleNames;

  public RolesNamespaceExtension() {
  }//constructor

  public void setRoleNames(Set s) {
    m_RoleNames = s;
  }//setRoleNames

  public String getNamespace() {
    return RolesExtension.NAMESPACE;
  }//getNamespace

  public void readExtensionSpecific(XMLStreamReader reader) throws XMLStreamException, MalformedStructureException, MalformedContentException {
    //not required.
  }//readExtensionSpecific

  public void writeExtensionSpecific(XMLStreamWriter writer) throws XMLStreamException {
    if(m_RoleNames != null && m_RoleNames.size()>0) {
      writer.writeStartElement(ELEMENT_ROLES);
      writer.writeNamespace("",RolesExtension.NAMESPACE);
      for(String role:m_RoleNames) {
        writer.writeStartElement(ELEMENT_ROLE);
        writer.writeCharacters(role);
        writer.writeEndElement();
      }
      writer.writeEndElement();
    }
  }//writeExtensionSpecific

  public void recycle() {
    m_RoleNames.clear();
  }//recycle

  private static final String ELEMENT_ROLES = "roles";
  private static final String ELEMENT_ROLE = "role";

}//class RolesNamespaceExtension
