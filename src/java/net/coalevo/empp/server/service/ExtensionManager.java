/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.service;

import net.coalevo.empp.server.model.ServerExtension;

/**
 * Defines an interface for the {@link ServerExtension} manager.
 * <p>
 * Note that any {@link ServerExtension} registered through the
 * OSGi service registration mechanism should automatically be
 * handled by the implementation (white-board model).
 * </p>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface ExtensionManager {

  /**
   * Registers a {@link ServerExtension}.
   *
   * @param ext an {@link ServerExtension} instance.
   * @return true if registered, false if not (duplicate).
   */
  public boolean register(ServerExtension ext);

  /**
   * Unregisters a {@link ServerExtension}.
   *
   * @param ns the namespace as <tt>String</tt>.
   * @return true if unregistered.
   */
  public boolean unregister(String ns);

}//interface ExtensionManager
