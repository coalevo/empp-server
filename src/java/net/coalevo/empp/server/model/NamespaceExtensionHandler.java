/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.model;

import net.coalevo.empp.model.NamespaceExtension;


/**
 * Defines the interface for a handler that can handle a specific
 * {@link NamespaceExtension}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface NamespaceExtensionHandler {

    /**
     * Handles a {@link NamespaceExtension} possibly returning a
     * response {@link NamespaceExtension}.
     *
     * @param ns the {@link NetworkSession}.
     * @param ext a {@link NamespaceExtension}.
     * @return a {@link NamespaceExtension} that should be added to a response.
     */
    public NamespaceExtension handleExtension(NetworkSession ns,NamespaceExtension ext);

}//interface NamespaceExtensionHandler
