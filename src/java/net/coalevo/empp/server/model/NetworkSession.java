/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.model;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.UserAgent;
import net.coalevo.presence.model.Presence;

/**
 * Defines the interface for the <tt>NetworkSession</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface NetworkSession {

  /**
   * Tests if this <tt>NetworkSession</tt> is authenticated.
   * @return true if authenticated, false otherwise.
   */
  public boolean isAuthenticated();

  /**
   * Returns the session identifier.
   *
   * @return the identifier of the network
   */
  public String getSessionIdentifier();

  /**
   * Returns the {@link AgentIdentifier} associated with
   * this <tt>NetworkSession</tt>.
   *
   * @return the associated {@link AgentIdentifier}.
   */
  public AgentIdentifier getAgentIdentifier();

  /**
   * Returns the {@link UserAgent} associated with this
   * <tt>NetworkSession</tt>.
   * @return the associated {@link UserAgent}.
   */
  public UserAgent getAgent();

  /**
   * Returns the {@link Presence} associated with this
   * <tt>NetworkSession</tt>.
   * @return the associated {@link Presence}.
   */
  public Presence getPresence();

  /**
   * Invalidates this <tt>NetworkSession</tt>.
   * <p>
   * Please use with care.
   * </p>
   */
  public void invalidate();

}//interface NetworkSession
