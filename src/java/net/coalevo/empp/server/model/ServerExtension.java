/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.server.model;

import net.coalevo.empp.model.NamespaceExtension;

/**
 * Defines an interface for EMPP extensions.
 * <p>
 * Each <tt>ServerExtension</tt> should provide a {@link NamespaceExtension}
 * and the corresponding {@link NamespaceExtensionHandler}.
 * </p>
 * <p>
 * The server or client should handle registering and unregistering
 * <tt>ServerExtension</tt> instances (white-board model) and integrate
 * them dynamically.
 * </p>
 *
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface ServerExtension {

  /**
    * Returns the specific namespace
    * for this <tt>ServerExtension</tt>.
    *
    * @return ns the namespace as <tt>String</tt>.
    */
   public String getNamespace();

  /**
   * Returns the {@link NamespaceExtension} provided by this
   * <tt>ServerExtension</tt>.
   *
   * @return a {@link NamespaceExtension}.
   */
  public Class getNamespaceExtension();

  /**
   * Returns the {@link NamespaceExtensionHandler} provided by this
   * <tt>ServerExtension</tt>.
   *
   * @return a {@link NamespaceExtensionHandler}.
   */
  public NamespaceExtensionHandler getExtensionHandler();

  /**
   * Tests if this <tt>ServerExtension</tt> provides a handler.
   * <p>
   * Some extensions could simply provide additional information
   * to the client, adding it to a presence, instant message or iq.
   * No handling at server side for this add-ons is required.
   * </p>
   *
   * @return true if handler required, false otherwise.
   */
  public boolean providesHandler();

}//interface ServerExtension
